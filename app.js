//web服务器
const express = require("express")
const url = require("url")

// 处理请求体中post传递的参数
const bodyParser = require("body-parser")


const indexRouter = require("./router/indexRouter")

//创建一个服务器应用
const app = express()
const port = 3000


//处理静态资源请求
app.use(express.static('public'))

// 同源策略
app.use((req, res, next) => {
    // console.log('拦截了');
    res.header('Access-Control-Allow-Origin', '*') // 允许任何域进行访问
    res.header('Access-Control-Allow-Headers', 'Authorization,X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method')
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PATCH, PUT, DELETE') // 允许请求的方式
    res.header('Allow', 'GET, POST, PATCH, OPTIONS, PUT, DELETE')
    next(); // 放行
});


//拦截所有请求
//extended:false 方法内部使用querystring模块处理请求参数的格式
//extended:true 方法内部使用第三方模块qs处理请求参数的格式

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}))


app.use('/', indexRouter)







app.listen(port, () => {
    console.log(`服务器已经启动在${port}端口`)
})